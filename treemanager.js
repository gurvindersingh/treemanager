/*!
	treemanager.js
	to manage Tree manager operations
	by Gurvinder Singh
	Date: March 29, 2015
*/

(function () {
	'use strict';
	
	//initialize object for treemanager functions
	var treemanager = {};
	
	/*
	 * Tree structure followed in plugin
	 *  	var tree = [
                    {depth: 0, nodes: [{id:1,parent:0}]},
                    {depth: 1, nodes: [{id:2,parent:1}, {id:3,parent:1}]},
                    {depth: 2, nodes: [{id:4,parent:2}, {id:5,parent:2}, {id:6,parent:3}]}
                ]
	*/
	
	
	/**
	 * Insert tree manager to an element
	 * @method showTreeManager
	 * @param {String} elementid 
	 */
	treemanager.showTreeManager = function (elementid){
		//check if sessionStorage has content
                if (sessionStorage.getItem("treeObj") != null) {
			//read json string from sessionstorage and convert to json
			var treejson = JSON.parse(sessionStorage.getItem("treeObj"));
			//convert json to html table
			var htm = treemanager.arrToTable(treejson);
			//insert html content to element on the page
			var ele = document.getElementById(elementid);
			
			if (document.contains(ele) == false)
				alert('Given element not found on page with id: #'+elementid);
			else
				ele.innerHTML = htm;
                }
	};
	
	/**
	 * Add new node to the tree
	 * @method addNode
	 * @param {Number} parentid
	 * @returns {Object} Array of object with new node.
	 */
	treemanager.addNode = function (parentid) {
		//check if storage is null
		if (sessionStorage.getItem("treeObj") != null) {
			//get current json from session storage
			var obj = JSON.parse(sessionStorage.getItem("treeObj"));
			
			//check if given parent exists in tree
			if((treemanager.findId(obj, parentid) === false && parentid != 0) || parentid == ''){
				alert('Parent not found.');
				return false;
			}
			
			//set an id for new element
			var newid = treemanager.getMaxID(obj) + 1;
			
			//set depth
			if (parentid == 0)
				var depth = 0;
			else
				var depth = treemanager.getDepth(obj, parentid) + 1;
				
			var depthExists = treemanager.findDepth(obj, depth);
			if (depthExists === false) {
				obj.push({depth: depth, nodes: []})
			}
			
			obj.forEach( function (val, index) {
				if(depth == val.depth ){
					//add element
					obj[index].nodes.push({id: newid, parent:parentid});
					//sort nodes by parent
					obj[index].nodes.sort(function(a, b){
						return a.parent > b.parent;
					});
				}
			});
		}
		else
		{
			if(parentid != 0 || parentid == ''){
				alert('Parent not found.');
				return false;
			}
			//if storage is empty add the first element to the tree
			obj = [{depth: 0, nodes: [{id: 1, parent:0}]}];
		}
		//update session storage with new tree
                sessionStorage.setItem('treeObj', JSON.stringify(obj));
		return obj;
	};
	
	/**
	 * Find a highest id in the array
	 * @method getMaxID
	 * @param {Object} arr
	 * @returns {Number} Highest id.
	 */
	treemanager.getMaxID = function (arr){
		var maxid = 0;
		arr.forEach( function (val, index) {
			val.nodes.forEach( function (nodeval) {                           
				if(maxid < nodeval.id)
					maxid = nodeval.id;
			});
		});
		return maxid;
	};
	
	/**
	 * Get depth by id
	 * @method getDepth
	 * @param {Object} arr
	 * @param {Number} id
	 * @returns {Number} Depth
	 */
	treemanager.getDepth = function (arr, id){
		var depth = 0;
		arr.forEach( function (val, index) {
			val.nodes.forEach( function (nodeval) {
				if(id == nodeval.id){
					depth = val.depth;
				}
			});
		});
		return depth;
	};
	
	/**
	 * Check if id value exists 
	 * @method findById
	 * @param {Object} arr
	 * @param {Number} id
	 * @returns {Boolean}
	 */	
	treemanager.findDepth = function (source, depth) {
		for (var i = 0; i < source.length; i++) {
			if (source[i].depth === depth) {
				return true;
			}
		}
		return false;
	};
	
	/**
	 * Check if id value exists 
	 * @method findById
	 * @param {Object} source
	 * @param {Number} id
	 * @returns {Boolean}
	 */	
	treemanager.findId = function (source, id) {
		for (var i = 0; i < source.length; i++) {
			for (var j = 0; j < source[i].nodes.length; j++) {
				if (source[i].nodes[j].id == id) {
					return true;
				}
			}
		}
		return false;
	};
	
	/**
	 * Check if all elements have same parent id
	 * @method checkforsameparent
	 * @param {Object} source
	 * @returns {Boolean}
	 */	
	treemanager.checkforsameparent = function (source) {
		for(var i = 1; i < source.length; i++)
		{
			if(source[i].parent != source[0].parent)
			    return false;
		}
		return true;
	};
	
	/**
	 * Convert tree structure into html table
	 * @method arrToTable
	 * @param {Object} obj
	 * @returns {String} Html content
	 */	
	treemanager.arrToTable = function(obj) {
		var htm = '<table><tr><th>Depth</th><th>Tree Nodes</th></tr>';
		var lastparent = 0;
		//check if given object contains content
		if (obj != null && obj.length > 0) {
			obj.forEach( function (val, index) {				
				htm += '<tr><td>'+val.depth+'</td><td>';
				if (val.nodes.length > 0){
					val.nodes.forEach( function (nodes, nodeindex) {
						var np = nodes.parent;					
						//set node parent NONE if 0
						if (np == 0)
							np = 'NONE';
						
						if ((lastparent != nodes.parent || treemanager.checkforsameparent(val.nodes) == true) && nodeindex != 0 )
						{
							htm += '<span class="pipeline">|</span>[P: '+np+' ID: '+nodes.id+']&nbsp;&nbsp;';
						}
						else
							htm += '[P: '+np+' ID: '+nodes.id+']&nbsp;&nbsp;';
							
						lastparent = nodes.parent;
					});
				}
				htm += '</td></tr>';
			});
			htm += '</table>';
			return htm;
		}
		else{
			return null;
		}
	};
	
	/**
	 * Clear sessionStorage data
	 * @method clearStorage
	 */
	treemanager.clearStorage = function (){
		sessionStorage.clear();
	}
	
	//define treemanager, globally and also if anonymous define() is called outside of a loader request
	if (typeof define === 'function' && define.amd) {
		define(function() { return treemanager; });
	} else {
		window.treemanager = treemanager;
	}
})();